package com.kdeepthi.go2movies.entity;

public interface Media {
	String getMovieName();
	void setMovieName(String MovieName);
	
	String getMediaType();
	void setMediaType(String MediaType);
	
	String getUrl();
	void setUrl(String Url);
	
	String getMediaString();
	
	long getID();
}
