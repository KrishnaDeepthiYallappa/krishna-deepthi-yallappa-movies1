package com.kdeepthi.go2movies.entity.impl;

import com.kdeepthi.go2movies.entity.User;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="users")
public class Userimpl implements User {
	@Id
	@Column(name="idusers")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long User_Id;
	
	@Column(name="username")
	private String UserName;
	
	@Column(name="firstname")
	private String FirstName;
	
	@Column(name="lastname")
	private String LastName;
	
	@Column(name="email")
	private String Email;
	
	@Column(name="phone")
	private String Phone;
	

	@Override
	public String getUsername() {
		return UserName;
	}

	@Override
	public void setUsername(String UserName) {
		this.UserName = UserName;
	}

	@Override
	public String getFirstname() {
		return FirstName;
	}

	@Override
	public void setFirstname(String FirstName) {
		this.FirstName = FirstName;
	}

	@Override
	public String getLastname() {
		return LastName;
	}

	@Override
	public void setLastname(String LastName) {
		this.LastName = LastName;
	}

	@Override
	public String getEmail() {
		return Email;
	}

	@Override
	public void setEmail(String Email) {
		this.Email = Email;
	}

	@Override
	public String getPhone() {
		return Phone;
	}

	@Override
	public void setPhone(String Phone) {
		this.Phone = Phone;
	}

	@Override
	public String getUserString() {
		return "User Name:" + this.UserName + 
				   " First Name:" + this.FirstName +
				   " Last Name:" + this.LastName +
				   " Email:" + this.Email +
				   " Phone:" + this.Phone;
	}

	@Override
	public long getID() {
		return this.User_Id;
	}
	
	

}
