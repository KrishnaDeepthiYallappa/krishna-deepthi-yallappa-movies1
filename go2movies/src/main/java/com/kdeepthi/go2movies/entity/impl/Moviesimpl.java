package com.kdeepthi.go2movies.entity.impl;

import com.kdeepthi.go2movies.entity.Movies;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="movielisting")
public class Moviesimpl implements Movies {
	@Id
	@Column(name="id_movie")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long Movie_Id;
	
	@Column(name="moviename")
	private String MovieName;
	
	@Column(name="movietiming")
	private int MovieTiming;
	
	@Column(name="theatername")
	private String TheaterName;
	

	@Override
	public String getMovieName() {
		return MovieName;
	}

	@Override
	public int getMovieTiming() {
		return MovieTiming;
	}

	@Override
	public String getTheaterName() {
		return TheaterName;
	}

	@Override
	public void SetMovieName(String MovieName) {
		this.MovieName = MovieName;

	}

	@Override
	public void SetMovieTiming(int MovieTiming) {
		this.MovieTiming = MovieTiming;

	}

	@Override
	public void SetTheaterName(String TheaterName) {
		this.TheaterName = TheaterName;

	}
	
	@Override
	public String GetMovieString(){
		return "Movie Name:" + this.MovieName + 
			   " Theater Name:" + this.TheaterName +
			   " Time:" + this.MovieTiming;
	}

	@Override
	public long getID() {
		return this.Movie_Id;
	}

}
