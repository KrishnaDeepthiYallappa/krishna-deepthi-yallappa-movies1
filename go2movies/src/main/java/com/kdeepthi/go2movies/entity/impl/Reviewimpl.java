package com.kdeepthi.go2movies.entity.impl;

import com.kdeepthi.go2movies.entity.Review;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="reviews")
public class Reviewimpl implements Review {
	@Id
	@Column(name="idreviews")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long Review_Id;
	
	@Column(name="moviename")
	private String MovieName;
	
	@Column(name="username")
	private String UserName;
	
	@Column(name="review")
	private String ReviewContent;
	
	@Column(name="rating")
	private int Rating;
	
	
	@Override
	public String getMovieName() {
		return MovieName;
	}

	@Override
	public void setMovieName(String MovieName) {
		this.MovieName = MovieName;

	}

	@Override
	public String getUserName() {
		return UserName;
	}

	@Override
	public void setUserName(String UserName) {
		this.UserName = UserName;

	}

	@Override
	public String getReviewContent() {
		return ReviewContent;
	}

	@Override
	public void setReview(String ReviewContent) {
		this.ReviewContent = ReviewContent;

	}

	@Override
	public int getRating() {
		return Rating;
	}

	@Override
	public void setRating(int Rating) {
		this.Rating = Rating;

	}

	@Override
	public String getReviewString() {
		return "Movie Name:" + this.MovieName + 
			   " User Name:" + this.UserName +
				   " Review:" + this.ReviewContent +
				   " Rating:" + this.Rating;
	}

	@Override
	public long getID() {
		return this.Review_Id;
	}

}
