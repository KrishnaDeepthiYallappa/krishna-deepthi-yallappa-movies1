package com.kdeepthi.go2movies.entity;


/**
 * Movie class
 * @author Deepthi
 *
 */
public interface Movies {
	
	/**
	 * Returns name of the movie
	 * @return
	 * Name of the movie
	 */
	String getMovieName();
	
	/**
	 * Returns the time of the movie
	 * @return
	 * Time of the movie
	 */
	int getMovieTiming();
	
	/**
	 * Returns theater name
	 * @return
	 * Name of the theater
	 */
	String getTheaterName();
	
	/**
	 * Sets the name of the movie
	 * @param MovieName
	 * Name of the movie
	 */
	void SetMovieName(String MovieName);
	
	/**
	 * Set timing of the movie 
	 * @param newtimings
	 * Timing object
	 */
	void SetMovieTiming(int newtimings);
	
	/**
	 * Set Theater Name
	 * @param TheaterName
	 * Name of the theater
	 */
	void SetTheaterName(String TheaterName);
	/**
	 * Returns movie info
	 * @return
	 * String concatenation of moviename, theatername and time
	 */
	String GetMovieString();
	
	long getID();
}
