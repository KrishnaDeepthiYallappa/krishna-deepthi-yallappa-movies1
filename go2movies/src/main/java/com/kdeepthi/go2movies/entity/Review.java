package com.kdeepthi.go2movies.entity;

public interface Review {
	String getMovieName();
	void setMovieName(String MovieName);
	
	String getUserName();
	void setUserName(String UserName);
	
	String getReviewContent();
	void setReview(String ReviewContent);
	
	int getRating();
	void setRating(int Rating); 
	
	String getReviewString();
	
	long getID();
}
