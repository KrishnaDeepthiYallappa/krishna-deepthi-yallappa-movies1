package com.kdeepthi.go2movies.entity.impl;

import com.kdeepthi.go2movies.entity.Media;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="media")
public class Mediaimpl implements Media {
	@Id
	@Column(name="idmedia")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long Media_Id;
	
	@Column(name="moviename")
	private String MovieName;
	
	@Column(name="mediatype")
	private String MediaType;
	
	@Column(name="url")
	private String Url;
	
	@Override
	public String getMovieName() {
		return MovieName;
	}

	@Override
	public void setMovieName(String MovieName) {
		this.MovieName = MovieName;

	}

	@Override
	public String getMediaType() {
		return MediaType;
	}

	@Override
	public void setMediaType(String MediaType) {
		this.MediaType = MediaType;

	}

	@Override
	public String getUrl() {
		return Url;
	}

	@Override
	public void setUrl(String Url) {
		this.Url = Url;

	}
	
	@Override
	public String getMediaString(){
		return "Movie Name:" + this.MovieName + 
				   " Media Type:" + this.MediaType +
					   " URL:" + this.Url;
	}

	@Override
	public long getID() {
		return Media_Id;
	}

}
