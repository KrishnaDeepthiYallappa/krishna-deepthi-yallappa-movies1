package com.kdeepthi.go2movies.entity;

public interface User {
	String getUsername();
	void setUsername(String UserName);
	
	String getFirstname();
	void setFirstname(String FirstName);
	
	String getLastname();
	void setLastname(String LastName);
	
	String getEmail();
	void setEmail(String Email);
	
	String getPhone();
	void setPhone(String Phone);
	
	String getUserString();
	
	long getID();
}
