package com.kdeepthi.go2movies.service;
import  java.util.List;

import com.kdeepthi.go2movies.entity.Media;


public interface MediaService {
	List<Media> getMediabyMovieName(String MovieName);
	Media addMedia(Media mymedia);
	int deleteMediabyName(String Moviename);
}
