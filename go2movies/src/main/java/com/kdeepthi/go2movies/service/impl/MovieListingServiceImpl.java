package com.kdeepthi.go2movies.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import com.kdeepthi.go2movies.entity.Movies;
import com.kdeepthi.go2movies.service.MovieListingService;
import com.kdeepthi.go2movies.repository.MovieRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class MovieListingServiceImpl implements MovieListingService {
	
	@Autowired
	private MovieRepository movieRepository;

	@Override
	public List<Movies> getTimingsbyMovieName(String MovieName) {
		// Hardcoding movie values. Need to read from database 
//		List<Movies> mymovielist = new ArrayList<Movies>();;
//		Movies mymovie1 = new Moviesimpl("Junglebook", 1300, "Century Great Mall");
//		Movies mymovie2 = new Moviesimpl("Junglebook", 1700, "AMC 20");
//		mymovielist.add(mymovie1);
//		mymovielist.add(mymovie2);
		List<Movies> mymovielist = movieRepository.getTimingsbyMovieName(MovieName);
		return mymovielist;
	}

	@Override
	public List<Movies> getTimingsbyTheater(String TheaterName) {
		// Hardcoding movie values. Need to read from database 
//		List<Movies> mymovielist = new ArrayList<Movies>();;
//		Movies mymovie1 = new Moviesimpl("Junglebook", 1300, "Century Great Mall");
//		Movies mymovie2 = new Moviesimpl("Fan", 1700, "Century Great Mall");
//		mymovielist.add(mymovie1);
//		mymovielist.add(mymovie2);
		List<Movies> mymovielist = movieRepository.getTimingsbyTheater(TheaterName);
		return mymovielist;
	}

	@Override
	public List<Movies> getMoviesbyTime(int Time) {
		// Hardcoding movie values. Need to read from database 
//		List<Movies> mymovielist = new ArrayList<Movies>();;
//		Movies mymovie1 = new Moviesimpl("Junglebook", 1700, "Century Great Mall");
//		Movies mymovie2 = new Moviesimpl("Fan", 1700, "AMC 16");
//		mymovielist.add(mymovie1);
//		mymovielist.add(mymovie2);
		List<Movies> mymovielist = movieRepository.getMoviesbyTime(Time);
		return mymovielist;
	}

	@Override
	public Movies AddMovieListing(Movies movielisting) {
		return movieRepository.AddMovieListing(movielisting);
	}

	@Override
	public int DeleteListingbyMovieName(String MovieName) {
		return movieRepository.DeleteListingbyMovieName(MovieName);
	}

	@Override
	public int DeleteListingbyTheaterName(String TheaterName) {
		return movieRepository.DeleteListingbyTheaterName(TheaterName);
	}

}
