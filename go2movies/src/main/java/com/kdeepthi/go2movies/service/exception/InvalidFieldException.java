package com.kdeepthi.go2movies.service.exception;

@SuppressWarnings("serial")
public class InvalidFieldException extends GTMException {
	public InvalidFieldException(String message, Throwable throwable) {
		super(ErrorCode.INVALID_FIELD, message, throwable);
	}
	
	public InvalidFieldException(String message) {
		super(ErrorCode.INVALID_FIELD, message);
	}
}
