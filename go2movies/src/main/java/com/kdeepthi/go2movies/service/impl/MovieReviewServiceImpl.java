package com.kdeepthi.go2movies.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import com.kdeepthi.go2movies.entity.Review;
import com.kdeepthi.go2movies.service.MovieReviewService;
import com.kdeepthi.go2movies.repository.MovieReviewRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class MovieReviewServiceImpl implements MovieReviewService {
	
	@Autowired
	private MovieReviewRepository moviereviewrepository;

	@Override
	public List<Review> getReviewsbyMovieName(String MovieName) {
		List<Review> moviereview = moviereviewrepository.getReviewsbyMovieName(MovieName);
		return moviereview;
	}

	@Override
	public List<Review> getReviewsbyUserName(String UserName) {
		List<Review> moviereview = moviereviewrepository.getReviewsbyUserName(UserName);
		return moviereview;
	}

	@Override
	public Review AddReview(Review newreview) {
		return moviereviewrepository.AddReview(newreview);
	}

	@Override
	public int DeleteReviewbyUsername(String Username) {
		return moviereviewrepository.DeleteReviewbyUsername(Username);
	}

	@Override
	public int DeleteReviewbyMoviename(String Moviename) {
		return moviereviewrepository.DeleteReviewbyMoviename(Moviename);
	}

}
