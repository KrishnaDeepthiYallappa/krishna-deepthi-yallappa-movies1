package com.kdeepthi.go2movies.service;
import  java.util.List;

import com.kdeepthi.go2movies.entity.Movies;


public interface MovieListingService {
	List<Movies> getTimingsbyMovieName(String MovieName);
	List<Movies> getTimingsbyTheater(String TheaterName);
	List<Movies> getMoviesbyTime(int Time);
	Movies AddMovieListing(Movies movielisting);
	int DeleteListingbyMovieName(String MovieName);
	int DeleteListingbyTheaterName(String TheaterName);
}
