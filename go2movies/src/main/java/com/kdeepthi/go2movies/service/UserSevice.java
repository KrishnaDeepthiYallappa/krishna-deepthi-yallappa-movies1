package com.kdeepthi.go2movies.service;
import  java.util.List;

import com.kdeepthi.go2movies.entity.User;

public interface UserSevice {
	List<User> getUserbyUseName(String UserName);
	User addUser(User newuser);
	int deleteUser(String Username);
}
