package com.kdeepthi.go2movies.service.exception;


@SuppressWarnings("serial")
public class GTMException extends RuntimeException {
	private ErrorCode errorCode;

	public GTMException(ErrorCode code, String message, Throwable throwable) {
		super(message, throwable);
		this.errorCode = code;
	}
	
	public GTMException(ErrorCode code, String message) {
		super(message);
		this.errorCode = code;
	}

	public ErrorCode getErrorCode() {
		return errorCode;
	}
}
