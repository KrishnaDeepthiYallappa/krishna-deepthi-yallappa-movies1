package com.kdeepthi.go2movies.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import com.kdeepthi.go2movies.entity.User;
import com.kdeepthi.go2movies.service.UserSevice;
import com.kdeepthi.go2movies.repository.UserRepository;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class UserServiceImpl implements UserSevice {
	
	@Autowired
	private UserRepository userrepository;

	@Override
	public List<User> getUserbyUseName(String UserName) {
		List<User> userlist = userrepository.getUserbyUserName(UserName);
		return userlist;
	}

	@Override
	public User addUser(User newuser) {
		return userrepository.addUser(newuser);
	}

	@Override
	public int deleteUser(String Username) {
		return userrepository.deleteUser(Username);
	}

}
