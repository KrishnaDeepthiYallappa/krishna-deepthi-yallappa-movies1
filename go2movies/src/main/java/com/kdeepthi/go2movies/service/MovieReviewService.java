package com.kdeepthi.go2movies.service;
import  java.util.List;

import com.kdeepthi.go2movies.entity.Review;

public interface MovieReviewService {
	List<Review> getReviewsbyMovieName(String MovieName);
	List<Review> getReviewsbyUserName(String UserName);
	Review AddReview(Review newreview);
	int DeleteReviewbyUsername(String Username);
	int DeleteReviewbyMoviename(String Moviename);
}
