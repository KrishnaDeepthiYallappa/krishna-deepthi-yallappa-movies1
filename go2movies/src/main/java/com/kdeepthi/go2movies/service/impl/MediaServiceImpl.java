package com.kdeepthi.go2movies.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import com.kdeepthi.go2movies.entity.Media;
import com.kdeepthi.go2movies.service.MediaService;
import com.kdeepthi.go2movies.repository.MediaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class MediaServiceImpl implements MediaService {
	
	@Autowired
	private MediaRepository mediarepository;

	@Override
	public List<Media> getMediabyMovieName(String MovieName) {
		List<Media> medialist = mediarepository.getMediabyMovieName(MovieName);
		return medialist;
	}

	@Override
	public Media addMedia(Media mymedia) {
		return mediarepository.addMedia(mymedia);
	}

	@Override
	public int deleteMediabyName(String Moviename) {
		return mediarepository.deleteMediabyName(Moviename);
	}

}
