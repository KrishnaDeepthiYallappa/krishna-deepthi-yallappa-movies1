package com.kdeepthi.go2movies.service.exception;

public enum ErrorCode {
	INVALID_FIELD,
	MISSING_DATA
}
