package com.kdeepthi.go2movies.http.entity;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.kdeepthi.go2movies.entity.Movies;

@XmlRootElement(name = "user")
public class HttpMovie {
	@XmlElement
	public String Moviename;
	
	@XmlElement
	public String Theatername;
	
	@XmlElement
	public int time;
	
	//required by framework
	public HttpMovie() {}

	public HttpMovie(Movies movie) {
		this.Moviename=movie.getMovieName();
		this.Theatername=movie.getTheaterName();
		this.time=movie.getMovieTiming();
	}
}
