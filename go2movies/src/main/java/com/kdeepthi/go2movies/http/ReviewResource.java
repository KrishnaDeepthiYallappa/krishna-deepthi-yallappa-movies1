package com.kdeepthi.go2movies.http;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.jboss.resteasy.annotations.providers.jaxb.Wrapped;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.kdeepthi.go2movies.entity.impl.Reviewimpl;
import com.kdeepthi.go2movies.entity.Review;
import com.kdeepthi.go2movies.http.entity.HttpReview;
import com.kdeepthi.go2movies.service.MovieReviewService;

@Path("/review")
@Component
@Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
public class ReviewResource {
private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private MovieReviewService movieReviewService;
	
	@POST
	@Path("/")
	public Response createReview(HttpReview HttpReview){
		Review reviewToCreate = convert(HttpReview);
		Review addedReview = movieReviewService.AddReview(reviewToCreate);
		return Response.status(Status.CREATED).header("Location", "/rewview/"+addedReview.getID()).entity(new HttpReview(addedReview)).build();
	}	
	
	@GET
	@Path("/moviename={moviename}")	
	public List<HttpReview> getReviewBymoviename(@PathParam("moviename") String moviename){
		logger.info("getting review by moviename:"+moviename);
		List<Review> found = movieReviewService.getReviewsbyMovieName(moviename);	
		List<HttpReview> returnList = new ArrayList<>(found.size());
		for(Review review:found){
			returnList.add(new HttpReview(review));
		}
		return returnList;
	}
	
	@GET
	@Path("/username={username}")	
	public List<HttpReview> getReviewByUsername(@PathParam("username") String username){
		logger.info("getting review by username:"+username);
		List<Review> found = movieReviewService.getReviewsbyUserName(username);	
		List<HttpReview> returnList = new ArrayList<>(found.size());
		for(Review review:found){
			returnList.add(new HttpReview(review));
		}
		return returnList;
	}
	
	@DELETE
	@Path("/moviename={moviename}")	
	public Response deleteReviewBymoviename(@PathParam("moviename") String moviename){
		logger.info("deleting review by moviename:"+moviename);
		int result = movieReviewService.DeleteReviewbyMoviename(moviename);
		if(result>0){
			return Response.status(Status.ACCEPTED).header("Result", "Successful").build();
		}
		else{
			return Response.status(Status.ACCEPTED).header("Result", "No such records").build();
		}
	}
	
	@DELETE
	@Path("/username={username}")	
	public Response deleteReviewByusername(@PathParam("username") String username){
		logger.info("deleting review by username:"+username);
		int result = movieReviewService.DeleteReviewbyUsername(username);
		if(result>0){
			return Response.status(Status.ACCEPTED).header("Result", "Successful").build();
		}
		else{
			return Response.status(Status.ACCEPTED).header("Result", "No such records").build();
		}
	}
	
	private Review convert(HttpReview httpReview) {
		Reviewimpl review = new Reviewimpl();
		review.setMovieName(httpReview.Moviename);
		review.setUserName(httpReview.Username);
		review.setReview(httpReview.Reviewcontent);
		review.setRating(httpReview.Rating);
		return review;
	}	
}
