package com.kdeepthi.go2movies.http.entity;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.kdeepthi.go2movies.entity.Media;

@XmlRootElement(name = "media")
public class HttpMedia {
	@XmlElement
	public String Moviename;
	
	@XmlElement
	public String Mediatype;
	
	@XmlElement
	public String url;
	
	//required by framework
	public HttpMedia() {}

	public HttpMedia(Media media) {
		this.Moviename=media.getMovieName();
		this.Mediatype=media.getMediaType();
		this.url=media.getUrl();
	}
}
