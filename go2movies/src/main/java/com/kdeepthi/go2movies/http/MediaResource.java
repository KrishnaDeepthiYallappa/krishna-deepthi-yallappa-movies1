package com.kdeepthi.go2movies.http;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.jboss.resteasy.annotations.providers.jaxb.Wrapped;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.kdeepthi.go2movies.entity.impl.Mediaimpl;
import com.kdeepthi.go2movies.entity.Media;
import com.kdeepthi.go2movies.http.entity.HttpMedia;
import com.kdeepthi.go2movies.service.MediaService;;

@Path("/media")
@Component
@Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
public class MediaResource {
private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private MediaService mediaService;
	
	@POST
	@Path("/")
	public Response createMedia(HttpMedia HttpMedia){
		Media mediaToCreate = convert(HttpMedia);
		Media addedMedia = mediaService.addMedia(mediaToCreate);
		return Response.status(Status.CREATED).header("Location", "/media/"+addedMedia.getID()).entity(new HttpMedia(addedMedia)).build();
	}	
	
	@GET
	@Path("/moviename={moviename}")	
	public List<HttpMedia> getMediaBymoviename(@PathParam("moviename") String moviename){
		logger.info("getting media by moviename:"+moviename);
		List<Media> found = mediaService.getMediabyMovieName(moviename);	
		List<HttpMedia> returnList = new ArrayList<>(found.size());
		for(Media media:found){
			returnList.add(new HttpMedia(media));
		}
		return returnList;
	}
	
	@DELETE
	@Path("/moviename={moviename}")	
	public Response deleteMediaBymoviename(@PathParam("moviename") String moviename){
		logger.info("deleting media by moviename:"+moviename);
		int result = mediaService.deleteMediabyName(moviename);
		if(result>0){
			return Response.status(Status.ACCEPTED).header("Result", "Successful").build();
		}
		else{
			return Response.status(Status.ACCEPTED).header("Result", "No such records").build();
		}
	}
		
	
	private Media convert(HttpMedia httpMedia) {
		Mediaimpl media = new Mediaimpl();
		media.setMovieName(httpMedia.Moviename);
		media.setMediaType(httpMedia.Mediatype);
		media.setUrl(httpMedia.url);
		return media;
	}	
}
