package com.kdeepthi.go2movies.http;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.jboss.resteasy.annotations.providers.jaxb.Wrapped;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.kdeepthi.go2movies.entity.impl.Moviesimpl;
import com.kdeepthi.go2movies.entity.Movies;
import com.kdeepthi.go2movies.http.entity.HttpMovie;
import com.kdeepthi.go2movies.service.MovieListingService;

@Path("/movies")
@Component
@Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
public class MovieResource {
private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private MovieListingService movieListingService;
	
	@POST
	@Path("/")
	public Response createMovie(HttpMovie HttpMovie){
		Movies movieToCreate = convert(HttpMovie);
		Movies addedMovie = movieListingService.AddMovieListing(movieToCreate);
		return Response.status(Status.CREATED).header("Location", "/movies/"+addedMovie.getID()).entity(new HttpMovie(addedMovie)).build();
	}	
	
	@GET
	@Path("/moviename={moviename}")	
	public List<HttpMovie> getMovieBymoviename(@PathParam("moviename") String moviename){
		logger.info("getting movie by moviename:"+moviename);
		List<Movies> found = movieListingService.getTimingsbyMovieName(moviename);	
		List<HttpMovie> returnList = new ArrayList<>(found.size());
		for(Movies movie:found){
			returnList.add(new HttpMovie(movie));
		}
		return returnList;
	}
	
	@GET
	@Path("/theatername={theatername}")	
	public List<HttpMovie> getMovieBytheatername(@PathParam("theatername") String theatername){
		logger.info("getting movie by moviename:"+theatername);
		List<Movies> found = movieListingService.getTimingsbyTheater(theatername);	
		List<HttpMovie> returnList = new ArrayList<>(found.size());
		for(Movies movie:found){
			returnList.add(new HttpMovie(movie));
		}
		return returnList;
	}
	
	@GET
	@Path("/time={time}")	
	public List<HttpMovie> getMovieBytime(@PathParam("time") int time){
		logger.info("getting movie by time:"+time);
		List<Movies> found = movieListingService.getMoviesbyTime(time);	
		List<HttpMovie> returnList = new ArrayList<>(found.size());
		for(Movies movie:found){
			returnList.add(new HttpMovie(movie));
		}
		return returnList;
	}
	
	@DELETE
	@Path("/moviename={moviename}")	
	public Response deleteMediaBymoviename(@PathParam("moviename") String moviename){
		logger.info("deleting movie by moviename:"+moviename);
		int result = movieListingService.DeleteListingbyMovieName(moviename);
		if(result>0){
			return Response.status(Status.ACCEPTED).header("Result", "Successful").build();
		}
		else{
			return Response.status(Status.ACCEPTED).header("Result", "No such records").build();
		}
	}
	
	@DELETE
	@Path("/theatername={theatername}")	
	public Response deleteMediaByTheatername(@PathParam("theatername") String theatername){
		logger.info("deleting movie by theatername:"+theatername);
		int result = movieListingService.DeleteListingbyTheaterName(theatername);
		if(result>0){
			return Response.status(Status.ACCEPTED).header("Result", "Successful").build();
		}
		else{
			return Response.status(Status.ACCEPTED).header("Result", "No such records").build();
		}
	}
	
	private Movies convert(HttpMovie httpMovie) {
		Moviesimpl movie = new Moviesimpl();
		movie.SetMovieName(httpMovie.Moviename);
		movie.SetTheaterName(httpMovie.Theatername);
		movie.SetMovieTiming(httpMovie.time);
		return movie;
	}	
}
