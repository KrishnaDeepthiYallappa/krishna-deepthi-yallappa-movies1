package com.kdeepthi.go2movies.http.entity;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.kdeepthi.go2movies.entity.User;

@XmlRootElement(name = "user")
public class HttpUser {
	
	@XmlElement
	public String userName;
	
	@XmlElement
	public String firstName;
	
	@XmlElement
	public String lastName;
	
	@XmlElement
	public String email;
	
	@XmlElement
	public String phone;
	
	//required by framework
	public HttpUser() {}

	public HttpUser(User user) {
		this.userName=user.getUsername();
		this.firstName=user.getFirstname();
		this.lastName=user.getLastname();
		this.email= user.getEmail();
		this.phone=user.getPhone();
	}
}
