package com.kdeepthi.go2movies.http.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.springframework.stereotype.Component;


import com.kdeepthi.go2movies.service.exception.GTMException;

@Provider
@Component
public class GTMExceptionMapper implements ExceptionMapper<GTMException> {

	@Override
	public Response toResponse(GTMException exception) {
		return Response.status(Status.CONFLICT).entity(new HttpError(exception)).build();
	}

}
