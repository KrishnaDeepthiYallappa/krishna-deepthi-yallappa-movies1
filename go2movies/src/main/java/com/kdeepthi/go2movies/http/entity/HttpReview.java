package com.kdeepthi.go2movies.http.entity;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.kdeepthi.go2movies.entity.Review;

@XmlRootElement(name = "review")
public class HttpReview {
	@XmlElement
	public String Moviename;
	
	@XmlElement
	public String Username;
	
	@XmlElement
	public String Reviewcontent;
	
	@XmlElement
	public int Rating;
	
	//required by framework
	public HttpReview() {}

	public HttpReview(Review review) {
		this.Moviename=review.getMovieName();
		this.Username=review.getUserName();
		this.Reviewcontent=review.getReviewContent();
		this.Rating=review.getRating();
	}
}
