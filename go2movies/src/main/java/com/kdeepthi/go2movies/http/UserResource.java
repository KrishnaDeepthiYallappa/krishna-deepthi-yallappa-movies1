package com.kdeepthi.go2movies.http;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.jboss.resteasy.annotations.providers.jaxb.Wrapped;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.kdeepthi.go2movies.entity.impl.Userimpl;
import com.kdeepthi.go2movies.entity.User;
import com.kdeepthi.go2movies.http.entity.HttpUser;
import com.kdeepthi.go2movies.service.UserSevice;


@Path("/users")
@Component
@Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
public class UserResource {
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private UserSevice userService;
	
	@POST
	@Path("/")
	public Response createUser(HttpUser newUser){
		User userToCreate = convert(newUser);
		User addedUser = userService.addUser(userToCreate);
		return Response.status(Status.CREATED).header("Location", "/users/"+addedUser.getID()).entity(new HttpUser(addedUser)).build();
	}	
	
	@GET
	@Path("/username={username}")	
	public List<HttpUser> getUserByusername(@PathParam("username") String username){
		logger.info("getting user by id:"+username);
		List<User> found = userService.getUserbyUseName(username);	
		List<HttpUser> returnList = new ArrayList<>(found.size());
		for(User user:found){
			returnList.add(new HttpUser(user));
		}
		return returnList;
	}
	
	@DELETE
	@Path("/username={username}")	
	public Response deleteUserByusername(@PathParam("username") String username){
		logger.info("deleting user by username:"+username);
		int result = userService.deleteUser(username);
		if(result>0){
			return Response.status(Status.ACCEPTED).header("Result", "Successful").build();
		}
		else{
			return Response.status(Status.ACCEPTED).header("Result", "No such records").build();
		}
	}
	
	private User convert(HttpUser httpUser) {
		Userimpl user = new Userimpl();
		user.setUsername(httpUser.userName);
		user.setFirstname(httpUser.firstName);
		user.setLastname(httpUser.lastName);
		user.setEmail(httpUser.email);
		user.setPhone(httpUser.phone);
		return user;
	}	
}
