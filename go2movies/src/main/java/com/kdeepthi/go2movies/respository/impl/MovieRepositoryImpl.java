package com.kdeepthi.go2movies.respository.impl;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

import com.kdeepthi.go2movies.entity.Movies;
import com.kdeepthi.go2movies.entity.impl.Moviesimpl;
import com.kdeepthi.go2movies.repository.MovieRepository;


@Repository
public class MovieRepositoryImpl implements MovieRepository {
	@Autowired
    private SessionFactory sessionFactory;
	
	@Override
	public List<Movies> getTimingsbyMovieName(String MovieName) {
		Criteria crit = this.sessionFactory.getCurrentSession().createCriteria(Moviesimpl.class)
				.add(Restrictions.eq("MovieName", MovieName));		
		List<Movies> Movietimings = crit.list();
		
		return Movietimings;
	}

	@Override
	public List<Movies> getTimingsbyTheater(String TheaterName) {
		Criteria crit = this.sessionFactory.getCurrentSession().createCriteria(Moviesimpl.class)
				.add(Restrictions.eq("TheaterName", TheaterName));		
		List<Movies> Movietimings = crit.list();
		
		return Movietimings;
	}

	@Override
	public List<Movies> getMoviesbyTime(int Time) {
		Criteria crit = this.sessionFactory.getCurrentSession().createCriteria(Moviesimpl.class)
				.add(Restrictions.eq("MovieTiming", Time));		
		List<Movies> Movietimings = crit.list();
		return Movietimings;
	}
	
	@Override
	public Movies getMoviesbyID(long id) {
		return (Movies) this.sessionFactory.getCurrentSession().get(Moviesimpl.class, id);
	}

	@Override
	public Movies AddMovieListing(Movies movietiming) {
		long newmovie =  (Long) this.sessionFactory.getCurrentSession().save(movietiming);	
		return getMoviesbyID(newmovie);
	}

	@Override
	public int DeleteListingbyMovieName(String MovieName) {
		String hql = "DELETE FROM Moviesimpl "  + 
	             "WHERE moviename = :moviename";
	Query query = this.sessionFactory.getCurrentSession().createQuery(hql);
	query.setParameter("moviename", MovieName);
	return query.executeUpdate();
	}

	@Override
	public int DeleteListingbyTheaterName(String TheaterName) {
		String hql = "DELETE FROM Moviesimpl "  + 
	             "WHERE theatername = :theatername";
	Query query = this.sessionFactory.getCurrentSession().createQuery(hql);
	query.setParameter("theatername", TheaterName);
	return query.executeUpdate();
	}

}
