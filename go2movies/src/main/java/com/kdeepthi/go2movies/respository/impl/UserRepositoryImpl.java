package com.kdeepthi.go2movies.respository.impl;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

import com.kdeepthi.go2movies.entity.User;
import com.kdeepthi.go2movies.entity.impl.Userimpl;
import com.kdeepthi.go2movies.repository.UserRepository;

@Repository
public class UserRepositoryImpl implements UserRepository {
	@Autowired
    private SessionFactory sessionFactory;

	@Override
	public List<User> getUserbyUserName(String UserName) {
		Criteria crit = this.sessionFactory.getCurrentSession().createCriteria(Userimpl.class)
				.add(Restrictions.eq("UserName", UserName));		
		List<User> UserList = crit.list();
		
		return UserList;
	}

	@Override
	public User getUserbyId(long id) {
		return (User) this.sessionFactory.getCurrentSession().get(Userimpl.class, id);
		
	}

	@Override
	public User addUser(User newuser) {
		long newuserid =  (Long) this.sessionFactory.getCurrentSession().save(newuser);	
		return getUserbyId(newuserid);
	}

	@Override
	public int deleteUser(String Username) {
		String hql = "DELETE FROM Userimpl "  + 
	             "WHERE username = :username";
	Query query = this.sessionFactory.getCurrentSession().createQuery(hql);
	query.setParameter("username", Username);
	return query.executeUpdate();
	}

}
