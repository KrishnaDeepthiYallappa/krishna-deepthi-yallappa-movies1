package com.kdeepthi.go2movies.respository.impl;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;


import com.kdeepthi.go2movies.entity.Media;
import com.kdeepthi.go2movies.entity.impl.Mediaimpl;
import com.kdeepthi.go2movies.repository.MediaRepository;

@Repository
public class MediaRepositoryImpl implements MediaRepository {
	@Autowired
    private SessionFactory sessionFactory;
	
	@Override
	public List<Media> getMediabyMovieName(String MovieName) {
		Criteria crit = this.sessionFactory.getCurrentSession().createCriteria(Mediaimpl.class)
				.add(Restrictions.eq("MovieName", MovieName));		
		List<Media> MediaList = crit.list();
		
		return MediaList;
	}

	@Override
	public Media addMedia(Media mymedia) {
		long newmedia =  (Long) this.sessionFactory.getCurrentSession().save(mymedia);	
		return getMediabyid(newmedia);
	}

	@Override
	public Media getMediabyid(long id) {
		return (Media) this.sessionFactory.getCurrentSession().get(Mediaimpl.class, id);
	}

	@Override
	public int deleteMediabyName(String Moviename) {
		String hql = "DELETE FROM Mediaimpl "  + 
	             "WHERE moviename = :moviename";
	Query query = this.sessionFactory.getCurrentSession().createQuery(hql);
	query.setParameter("moviename", Moviename);
	return query.executeUpdate();
	}
}
