package com.kdeepthi.go2movies.respository.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.kdeepthi.go2movies.entity.Review;
import com.kdeepthi.go2movies.entity.impl.Reviewimpl;
import com.kdeepthi.go2movies.repository.MovieReviewRepository;

@Repository
public class MovieReviewRepositoryImpl implements MovieReviewRepository {
	@Autowired
    private SessionFactory sessionFactory;

	@Override
	public List<Review> getReviewsbyMovieName(String MovieName) {
		Criteria crit = this.sessionFactory.getCurrentSession().createCriteria(Review.class)
				.add(Restrictions.eq("MovieName", MovieName));		
		List<Review> Moviereviews = crit.list();
		
		return Moviereviews;
	}

	@Override
	public List<Review> getReviewsbyUserName(String UserName) {
		Criteria crit = this.sessionFactory.getCurrentSession().createCriteria(Review.class)
				.add(Restrictions.eq("UserName", UserName));		
		List<Review> Moviereviews = crit.list();
		
		return Moviereviews;
	}
	
	@Override
	public Review getReviewsbyid(long id) {
		return (Review) this.sessionFactory.getCurrentSession().get(Reviewimpl.class, id);
		
	}

	@Override
	public Review AddReview(Review newreview) {
		long newreviewid =  (Long) this.sessionFactory.getCurrentSession().save(newreview);	
		return getReviewsbyid(newreviewid);
	}

	@Override
	public int DeleteReviewbyUsername(String Username) {
		String hql = "DELETE FROM Reviewimpl "  + 
	             "WHERE username = :username";
	Query query = this.sessionFactory.getCurrentSession().createQuery(hql);
	query.setParameter("username", Username);
	return query.executeUpdate();
	}

	@Override
	public int DeleteReviewbyMoviename(String Moviename) {
		String hql = "DELETE FROM Reviewimpl "  + 
	             "WHERE moviename = :moviename";
	Query query = this.sessionFactory.getCurrentSession().createQuery(hql);
	query.setParameter("moviename", Moviename);
	return query.executeUpdate();
	}


}
