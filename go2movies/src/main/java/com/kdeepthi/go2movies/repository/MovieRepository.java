package com.kdeepthi.go2movies.repository;

import java.util.List;


import com.kdeepthi.go2movies.entity.Movies;

public interface MovieRepository {
	public List<Movies> getTimingsbyMovieName(String MovieName);
	public List<Movies> getTimingsbyTheater(String TheaterName);
	public List<Movies> getMoviesbyTime(int Time);
	Movies getMoviesbyID(long id);
	Movies AddMovieListing(Movies movietiming);
	int DeleteListingbyMovieName(String MovieName);
	int DeleteListingbyTheaterName(String TheaterName);
}
