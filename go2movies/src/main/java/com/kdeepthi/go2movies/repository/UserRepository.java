package com.kdeepthi.go2movies.repository;

import java.util.List;

import com.kdeepthi.go2movies.entity.User;

public interface UserRepository {
	List<User> getUserbyUserName(String UserName);
	User getUserbyId(long id);
	User addUser(User newuser);
	int deleteUser(String Username);
}
