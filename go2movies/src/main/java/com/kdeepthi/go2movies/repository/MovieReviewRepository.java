package com.kdeepthi.go2movies.repository;

import java.util.List;

import com.kdeepthi.go2movies.entity.Review;

public interface MovieReviewRepository {
	List<Review> getReviewsbyMovieName(String MovieName);
	List<Review> getReviewsbyUserName(String UserName);
	Review getReviewsbyid(long id);
	Review AddReview(Review newreview);
	int DeleteReviewbyUsername(String Username);
	int DeleteReviewbyMoviename(String Moviename);
}
