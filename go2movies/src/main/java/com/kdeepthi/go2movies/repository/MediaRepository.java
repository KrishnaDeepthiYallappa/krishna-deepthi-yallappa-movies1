package com.kdeepthi.go2movies.repository;

import java.util.List;

import com.kdeepthi.go2movies.entity.Media;

public interface MediaRepository {
	List<Media> getMediabyMovieName(String MovieName);
	Media addMedia(Media mymedia);
	Media getMediabyid(long id);
	int deleteMediabyName(String Moviename);
}
