package com.kdeepthi.go2movies.http;

import java.util.List;
import java.util.Random;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.kdeepthi.go2movies.http.entity.HttpUser;
import com.kdeepthi.go2movies.http.exception.HttpError;
import com.kdeepthi.go2movies.entity.User;
/**
 * Test error scenarios using a Client
 * @author deepthi
 *
 */
public class TestUserResource {
	private static final String HTTP_HOST = "http://localhost:8080";
	private static final String URI_PATH = "go2movies/rest/users";
	
	private Client client = ClientBuilder.newClient();
	private WebTarget target;
	
	@Before
	public void init(){
		target = client.target(HTTP_HOST).path(URI_PATH);
	}

	
	// Writing test to add, retrieve and delete a user.
	@Test
	public void testCreateAndGetUser(){		
		//Create new user
		HttpUser userToSend = new HttpUser();
		userToSend.firstName="foo"+new Random().nextInt(99999);
		userToSend.lastName="bar"+new Random().nextInt(99999);;
		userToSend.userName=userToSend.firstName+userToSend.lastName;
		userToSend.email="foo@bar.com";
		userToSend.phone="555-555-5555";
		
		Response response =	target.request().accept(MediaType.APPLICATION_JSON).post(Entity.entity(userToSend, MediaType.APPLICATION_JSON));
		
		//verify response
		HttpUser createResponse = response.readEntity(HttpUser.class);
		//System.err.println(createResponse);
		Assert.assertEquals(201, response.getStatus());
		Assert.assertEquals(createResponse.userName, userToSend.userName);
		Assert.assertEquals(createResponse.firstName, userToSend.firstName);
		Assert.assertEquals(createResponse.lastName, userToSend.lastName);
		
		//search for user
		target = client.target(HTTP_HOST).path(URI_PATH+"/username="+userToSend.userName);
		Response search = target.request().accept(MediaType.APPLICATION_JSON).get();
		List<HttpUser> searchResponse = search.readEntity(new GenericType<List<HttpUser>>(){});
		Assert.assertEquals(1,  searchResponse.size());
		Assert.assertEquals(searchResponse.get(0).userName, createResponse.userName);
		Assert.assertEquals(searchResponse.get(0).firstName, createResponse.firstName);
		Assert.assertEquals(searchResponse.get(0).lastName, createResponse.lastName);
		
		//delete user
		target = client.target(HTTP_HOST).path(URI_PATH+"/username="+userToSend.userName);
		Response deleted = target.request().delete();
		Assert.assertEquals(deleted.getStatus(),202);
		
	}
}

