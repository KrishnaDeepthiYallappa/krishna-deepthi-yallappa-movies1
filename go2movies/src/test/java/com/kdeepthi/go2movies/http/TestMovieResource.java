package com.kdeepthi.go2movies.http;

import java.util.List;
import java.util.Random;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.kdeepthi.go2movies.http.entity.HttpMovie;
import com.kdeepthi.go2movies.http.exception.HttpError;
import com.kdeepthi.go2movies.entity.User;
/**
 * Test error scenarios using a Client
 * @author rahul
 *
 */
public class TestMovieResource {
	private static final String HTTP_HOST = "http://localhost:8080";
	private static final String URI_PATH = "go2movies/rest/movies";
	
	private Client client = ClientBuilder.newClient();
	private WebTarget target;
	
	@Before
	public void init(){
		target = client.target(HTTP_HOST).path(URI_PATH);
	}

	
	// Writing test to add, retrieve and delete a user.
	@Test
	public void testCreateAndGetUser(){		
		//Create new user
		HttpMovie movieToSend = new HttpMovie();
		movieToSend.Moviename="foo"+new Random().nextInt(99999);
		movieToSend.Theatername="bar"+new Random().nextInt(99999);
		movieToSend.time=1200;
		
		
		Response response =	target.request().accept(MediaType.APPLICATION_JSON).post(Entity.entity(movieToSend, MediaType.APPLICATION_JSON));
		
		//verify response
		HttpMovie createResponse = response.readEntity(HttpMovie.class);
		//System.err.println(createResponse);
		Assert.assertEquals(201, response.getStatus());
		Assert.assertEquals(createResponse.Moviename, movieToSend.Moviename);
		Assert.assertEquals(createResponse.Theatername, movieToSend.Theatername);
		Assert.assertEquals(createResponse.time, movieToSend.time);
		
		//search for movie with moviename
		target = client.target(HTTP_HOST).path(URI_PATH+"/moviename="+movieToSend.Moviename);
		Response search = target.request().accept(MediaType.APPLICATION_JSON).get();
		List<HttpMovie> searchResponse = search.readEntity(new GenericType<List<HttpMovie>>(){});
		Assert.assertEquals(1,  searchResponse.size());
		Assert.assertEquals(searchResponse.get(0).Moviename, createResponse.Moviename);
		Assert.assertEquals(searchResponse.get(0).Theatername, createResponse.Theatername);
		Assert.assertEquals(searchResponse.get(0).time, createResponse.time);
		
		//search for movie with theatername
		target = client.target(HTTP_HOST).path(URI_PATH+"/theatername="+movieToSend.Theatername);
		Response search1 = target.request().accept(MediaType.APPLICATION_JSON).get();
		List<HttpMovie> searchResponse1 = search1.readEntity(new GenericType<List<HttpMovie>>(){});
		Assert.assertEquals(1,  searchResponse1.size());
		Assert.assertEquals(searchResponse1.get(0).Moviename, createResponse.Moviename);
		Assert.assertEquals(searchResponse1.get(0).Theatername, createResponse.Theatername);
		Assert.assertEquals(searchResponse1.get(0).time, createResponse.time);
		
		//search for movie with time
		target = client.target(HTTP_HOST).path(URI_PATH+"/time="+movieToSend.time);
		Response search2 = target.request().accept(MediaType.APPLICATION_JSON).get();
		List<HttpMovie> searchResponse2 = search2.readEntity(new GenericType<List<HttpMovie>>(){});
		Assert.assertEquals(searchResponse2.get(0).time, createResponse.time);
		
		
		//delete movie
		target = client.target(HTTP_HOST).path(URI_PATH+"/moviename="+movieToSend.Moviename);
		Response deleted = target.request().delete();
		Assert.assertEquals(deleted.getStatus(),202);
		
	}
}

