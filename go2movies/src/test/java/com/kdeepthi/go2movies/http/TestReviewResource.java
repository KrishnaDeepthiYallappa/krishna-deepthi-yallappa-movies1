package com.kdeepthi.go2movies.http;

import java.util.List;
import java.util.Random;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.kdeepthi.go2movies.http.entity.HttpReview;
import com.kdeepthi.go2movies.http.exception.HttpError;
import com.kdeepthi.go2movies.entity.Media;
/**
 * Test error scenarios using a Client
 * @author deepthi
 *
 */
public class TestReviewResource {
	private static final String HTTP_HOST = "http://localhost:8080";
	private static final String URI_PATH = "go2movies/rest/review";
	
	private Client client = ClientBuilder.newClient();
	private WebTarget target;
	
	@Before
	public void init(){
		target = client.target(HTTP_HOST).path(URI_PATH);
	}

	
	// Writing test to add, retrieve and delete a user.
	@Test
	public void testCreateAndGetMedia(){		
		//Create new media
		HttpReview reviewToSend = new HttpReview();
		
		reviewToSend.Moviename="foo"+new Random().nextInt(99999);
		reviewToSend.Username="bar"+new Random().nextInt(99999);
		reviewToSend.Reviewcontent="bkabclajebc"+new Random().nextInt(99999);
		reviewToSend.Rating=new Random().nextInt(5);
		
		
		
		Response response =	target.request().accept(MediaType.APPLICATION_JSON).post(Entity.entity(reviewToSend, MediaType.APPLICATION_JSON));
		
		//verify response
		HttpReview createResponse = response.readEntity(HttpReview.class);
		//System.err.println(createResponse);
		Assert.assertEquals(201, response.getStatus());
		Assert.assertEquals(createResponse.Moviename, reviewToSend.Moviename);
		Assert.assertEquals(createResponse.Username, reviewToSend.Username);
		Assert.assertEquals(createResponse.Reviewcontent, reviewToSend.Reviewcontent);
		Assert.assertEquals(createResponse.Rating, reviewToSend.Rating);
		
		//search for media with moviename
		target = client.target(HTTP_HOST).path(URI_PATH+"/moviename="+reviewToSend.Moviename);
		Response search = target.request().accept(MediaType.APPLICATION_JSON).get();
		List<HttpReview> searchResponse = search.readEntity(new GenericType<List<HttpReview>>(){});
		Assert.assertEquals(1,  searchResponse.size());
		Assert.assertEquals(searchResponse.get(0).Moviename, createResponse.Moviename);
		Assert.assertEquals(searchResponse.get(0).Username, createResponse.Username);
		Assert.assertEquals(searchResponse.get(0).Reviewcontent, createResponse.Reviewcontent);
		Assert.assertEquals(searchResponse.get(0).Rating, createResponse.Rating);
		
		//search for media with username
		target = client.target(HTTP_HOST).path(URI_PATH+"/username="+reviewToSend.Username);
		Response search1 = target.request().accept(MediaType.APPLICATION_JSON).get();
		List<HttpReview> searchResponse1 = search1.readEntity(new GenericType<List<HttpReview>>(){});
		Assert.assertEquals(1,  searchResponse1.size());
		Assert.assertEquals(searchResponse1.get(0).Moviename, createResponse.Moviename);
		Assert.assertEquals(searchResponse1.get(0).Username, createResponse.Username);
		Assert.assertEquals(searchResponse1.get(0).Reviewcontent, createResponse.Reviewcontent);
		Assert.assertEquals(searchResponse1.get(0).Rating, createResponse.Rating);
		
		//Delete media
		target = client.target(HTTP_HOST).path(URI_PATH+"/moviename="+reviewToSend.Moviename);
		Response deleted = target.request().delete();
		Assert.assertEquals(deleted.getStatus(),202);
		
	}
}

