package com.kdeepthi.go2movies.http;

import java.util.List;
import java.util.Random;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.kdeepthi.go2movies.http.entity.HttpMedia;
import com.kdeepthi.go2movies.http.exception.HttpError;
import com.kdeepthi.go2movies.entity.Media;
/**
 * Test error scenarios using a Client
 * @author deepthi
 *
 */
public class TestMediaResource {
	private static final String HTTP_HOST = "http://localhost:8080";
	private static final String URI_PATH = "go2movies/rest/media";
	
	private Client client = ClientBuilder.newClient();
	private WebTarget target;
	
	@Before
	public void init(){
		target = client.target(HTTP_HOST).path(URI_PATH);
	}

	
	// Writing test to add, retrieve and delete a user.
	@Test
	public void testCreateAndGetMedia(){		
		//Create new media
		HttpMedia mediaToSend = new HttpMedia();
		
		mediaToSend.Moviename="foo"+new Random().nextInt(99999);
		mediaToSend.Mediatype="image";
		mediaToSend.url="bar"+new Random().nextInt(99999);
		
		
		
		Response response =	target.request().accept(MediaType.APPLICATION_JSON).post(Entity.entity(mediaToSend, MediaType.APPLICATION_JSON));
		
		//verify response
		HttpMedia createResponse = response.readEntity(HttpMedia.class);
		//System.err.println(createResponse);
		Assert.assertEquals(201, response.getStatus());
		Assert.assertEquals(createResponse.Moviename, mediaToSend.Moviename);
		Assert.assertEquals(createResponse.Mediatype, mediaToSend.Mediatype);
		Assert.assertEquals(createResponse.url, mediaToSend.url);
		
		//search for media
		target = client.target(HTTP_HOST).path(URI_PATH+"/moviename="+mediaToSend.Moviename);
		Response search = target.request().accept(MediaType.APPLICATION_JSON).get();
		List<HttpMedia> searchResponse = search.readEntity(new GenericType<List<HttpMedia>>(){});
		Assert.assertEquals(1,  searchResponse.size());
		Assert.assertEquals(searchResponse.get(0).Moviename, createResponse.Moviename);
		Assert.assertEquals(searchResponse.get(0).Mediatype, createResponse.Mediatype);
		Assert.assertEquals(searchResponse.get(0).url, createResponse.url);
		
		//Delete media
		target = client.target(HTTP_HOST).path(URI_PATH+"/moviename="+mediaToSend.Moviename);
		Response deleted = target.request().delete();
		Assert.assertEquals(deleted.getStatus(),202);
		
	}
}

