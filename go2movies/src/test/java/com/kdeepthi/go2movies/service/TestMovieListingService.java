package com.kdeepthi.go2movies.service;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import com.kdeepthi.go2movies.entity.Movies;
import com.kdeepthi.go2movies.entity.impl.Moviesimpl;
import com.kdeepthi.go2movies.service.MovieListingService;

@ContextConfiguration(locations = {"classpath:spring-context.xml"})
public class TestMovieListingService extends AbstractJUnit4SpringContextTests {
	@Autowired
	private MovieListingService movielistingservice;
	
	@Test
	public void gettimingsbyname(){
		List<Movies> movielist = movielistingservice.getTimingsbyMovieName("Junglebook");
		System.out.println("================Moviename: Junglebook===========");
		for(int i=0; i<movielist.size(); i++){
			System.out.println(movielist.get(i).GetMovieString());
			Assert.assertEquals(movielist.get(i).getMovieName(), "Junglebook");
		}
	}
	
	@Test
	public void gettimingsbytheater(){
		List<Movies> movielist = movielistingservice.getTimingsbyTheater("Century Greatmall");
		System.out.println("================Theatername: Century Greatmall===========");
		for(int i=0; i<movielist.size(); i++){
			System.out.println(movielist.get(i).GetMovieString());
			Assert.assertEquals(movielist.get(i).getTheaterName(), "Century Greatmall");
		}
	}
	
	@Test
	public void getmoviesbytimings(){
		List<Movies> movielist = movielistingservice.getMoviesbyTime(1800);
		System.out.println("================Time: 1800===========");
		for(int i=0; i<movielist.size(); i++){
			System.out.println(movielist.get(i).GetMovieString());
			Assert.assertEquals(movielist.get(i).getMovieTiming(), 1800);
		}
	}
	
	@Test
	public void addlisting(){
		Moviesimpl movielisting = new Moviesimpl();
		String MovieName = "Brahmotsavam";
		String TheaterName = "Serra Theatre";
		int MovieTiming = 2030;
		movielisting.SetMovieName(MovieName);
		movielisting.SetTheaterName(TheaterName);
		movielisting.SetMovieTiming(MovieTiming);
		Movies newlisting = movielistingservice.AddMovieListing(movielisting);
		Assert.assertEquals(newlisting.getMovieName(),MovieName);
		Assert.assertEquals(newlisting.getTheaterName(),TheaterName);
		Assert.assertEquals(newlisting.getMovieTiming(),MovieTiming);
	}
	
	@Test
	public void deletemediabymoviename(){
		Moviesimpl movielisting = new Moviesimpl();
		String MovieName = "test";
		String TheaterName = "test";
		int MovieTiming = 2030;
		movielisting.SetMovieName(MovieName);
		movielisting.SetTheaterName(TheaterName);
		movielisting.SetMovieTiming(MovieTiming);
		Movies newlisting = movielistingservice.AddMovieListing(movielisting);
		int result = movielistingservice.DeleteListingbyMovieName(MovieName);
		Assert.assertEquals(result,1);
	}
	
	@Test
	public void deletemediabytheatername(){
		Moviesimpl movielisting = new Moviesimpl();
		String MovieName = "test";
		String TheaterName = "test";
		int MovieTiming = 2030;
		movielisting.SetMovieName(MovieName);
		movielisting.SetTheaterName(TheaterName);
		movielisting.SetMovieTiming(MovieTiming);
		Movies newlisting = movielistingservice.AddMovieListing(movielisting);
		int result = movielistingservice.DeleteListingbyTheaterName(TheaterName);
		Assert.assertEquals(result,1);
	}
}
