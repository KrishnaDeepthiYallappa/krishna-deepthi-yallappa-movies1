package com.kdeepthi.go2movies.service;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import com.kdeepthi.go2movies.entity.Review;
import com.kdeepthi.go2movies.entity.impl.Reviewimpl;
import com.kdeepthi.go2movies.service.MovieReviewService;

@ContextConfiguration(locations = {"classpath:spring-context.xml"})
public class TestMovieReviewService extends AbstractJUnit4SpringContextTests {
	@Autowired
	private MovieReviewService moviereviewservice;
	
	@Test
	public void getreviewbymoviename(){
		List<Review> reviewlist = moviereviewservice.getReviewsbyMovieName("Junglebook");
		System.out.println("================Moviename: Junglebook===========");
		for(int i=0; i<reviewlist.size(); i++){
			System.out.println(reviewlist.get(i).getReviewString());
			Assert.assertEquals(reviewlist.get(i).getMovieName(), "Junglebook");
		}
	}
	
	@Test
	public void getreviewbyusername(){
		List<Review> reviewlist = moviereviewservice.getReviewsbyUserName("krishnacp");
		System.out.println("================Username: krishnacp===========");
		for(int i=0; i<reviewlist.size(); i++){
			System.out.println(reviewlist.get(i).getReviewString());
			Assert.assertEquals(reviewlist.get(i).getUserName(), "krishnacp");
		}
	}
	
	@Test
	public void addreview(){
		Reviewimpl newreview = new Reviewimpl();
		String MovieName = "Brahmotsavam";
		String UserName = "krishnacp";
		String ReviewContent = "Family oriented movie";
		int Rating = 5;
		newreview.setMovieName(MovieName);
		newreview.setUserName(UserName);
		newreview.setReview(ReviewContent);
		newreview.setRating(Rating);
		Review reviewresponse = moviereviewservice.AddReview(newreview);
		Assert.assertEquals(reviewresponse.getMovieName(),MovieName);
		Assert.assertEquals(reviewresponse.getUserName(),UserName);
		Assert.assertEquals(reviewresponse.getReviewContent(),ReviewContent);
		Assert.assertEquals(reviewresponse.getRating(),Rating);
	}
	
	@Test
	public void deletereviewbymoviename(){
		Reviewimpl newreview = new Reviewimpl();
		String MovieName = "test";
		String UserName = "test";
		String ReviewContent = "test";
		int Rating = 5;
		newreview.setMovieName(MovieName);
		newreview.setUserName(UserName);
		newreview.setReview(ReviewContent);
		newreview.setRating(Rating);
		Review reviewresponse = moviereviewservice.AddReview(newreview);
		int result = moviereviewservice.DeleteReviewbyMoviename(MovieName);
		Assert.assertEquals(result,1);
	}
		
	@Test
	public void deletereviewbytheatername(){
		Reviewimpl newreview = new Reviewimpl();
		String MovieName = "test";
		String UserName = "test";
		String ReviewContent = "test";
		int Rating = 5;
		newreview.setMovieName(MovieName);
		newreview.setUserName(UserName);
		newreview.setReview(ReviewContent);
		newreview.setRating(Rating);
		Review reviewresponse = moviereviewservice.AddReview(newreview);
		int result = moviereviewservice.DeleteReviewbyUsername(UserName);
		Assert.assertEquals(result,1);
	}

}
