package com.kdeepthi.go2movies.service;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import com.kdeepthi.go2movies.entity.User;
import com.kdeepthi.go2movies.entity.impl.Userimpl;
import com.kdeepthi.go2movies.service.UserSevice;

@ContextConfiguration(locations = {"classpath:spring-context.xml"})
public class TestUserService extends AbstractJUnit4SpringContextTests {
	@Autowired
	private UserSevice userservice;
	
	@Test
	public void getuserbyusername(){
		List<User> userlist = userservice.getUserbyUseName("krishnacp");
		System.out.println("================Username: krishnacp===========");
		for(int i=0; i<userlist.size(); i++){
			System.out.println(userlist.get(i).getUserString());
			Assert.assertEquals(userlist.get(i).getUsername(), "krishnacp");
		}
	}
	
	@Test
	public void adduser(){
		Userimpl newuser = new Userimpl();
		String Username = "Testuser";
		String Firstname = "Testfirstname";
		String Lastname = "Testlastname";
		String Email = "test@test.com";
		String Phone = "555-555-5555"; 
		newuser.setUsername(Username);
		newuser.setFirstname(Firstname);
		newuser.setLastname(Lastname);
		newuser.setEmail(Email);
		newuser.setPhone(Phone);
		User userresponse = userservice.addUser(newuser);
		Assert.assertEquals(userresponse.getUsername(),Username);
		Assert.assertEquals(userresponse.getFirstname(),Firstname);
		Assert.assertEquals(userresponse.getLastname(),Lastname);
		Assert.assertEquals(userresponse.getEmail(),Email);
		Assert.assertEquals(userresponse.getPhone(),Phone);
	}
	
	@Test
	public void deleteuserbyusername(){
		Userimpl newuser = new Userimpl();
		String Username = "test";
		String Firstname = "test";
		String Lastname = "test";
		String Email = "test@test.com";
		String Phone = "test"; 
		newuser.setUsername(Username);
		newuser.setFirstname(Firstname);
		newuser.setLastname(Lastname);
		newuser.setEmail(Email);
		newuser.setPhone(Phone);
		User userresponse = userservice.addUser(newuser);
		int result = userservice.deleteUser(Username);
		Assert.assertEquals(result,1);
	}
	
}
