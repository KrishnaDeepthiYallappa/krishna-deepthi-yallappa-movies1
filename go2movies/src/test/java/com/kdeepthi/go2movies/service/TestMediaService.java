package com.kdeepthi.go2movies.service;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import com.kdeepthi.go2movies.entity.Media;
import com.kdeepthi.go2movies.entity.impl.Mediaimpl;
import com.kdeepthi.go2movies.service.MediaService;

@ContextConfiguration(locations = {"classpath:spring-context.xml"})
public class TestMediaService extends AbstractJUnit4SpringContextTests {
	@Autowired
	private MediaService mediaservice;
	
	@Test
	public void getmediabymoviename(){
		List<Media> medialist = mediaservice.getMediabyMovieName("Junglebook");
		System.out.println("================Moviename: Junglebook===========");
		for(int i=0; i<medialist.size(); i++){
			System.out.println(medialist.get(i).getMediaString());
			Assert.assertEquals(medialist.get(i).getMovieName(), "Junglebook");
		}
	}
	
	@Test
	public void addmedia(){
		Mediaimpl mymedia = new Mediaimpl();
		String Moviename = "Brahmotsavam";
		String Mediatype = "Video";
		String Mediaurl = "https://www.youtube.com/watch?v=cWRYYZjCMgY";
		mymedia.setMovieName(Moviename);
		mymedia.setMediaType(Mediatype);
		mymedia.setUrl(Mediaurl);
		Media newmedia = mediaservice.addMedia(mymedia);
		Assert.assertEquals(newmedia.getMovieName(),Moviename);
		Assert.assertEquals(newmedia.getMediaType(),Mediatype);
		Assert.assertEquals(newmedia.getUrl(),Mediaurl);
	}
	
	@Test
	public void deletemedia(){
		Mediaimpl mymedia = new Mediaimpl();
		String Moviename = "test";
		String Mediatype = "test";
		String Mediaurl = "test";
		mymedia.setMovieName(Moviename);
		mymedia.setMediaType(Mediatype);
		mymedia.setUrl(Mediaurl);
		Media newmedia = mediaservice.addMedia(mymedia);
		int result = mediaservice.deleteMediabyName(Moviename);
		Assert.assertEquals(result,1);
	}
}
